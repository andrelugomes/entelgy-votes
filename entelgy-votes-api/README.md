# Entelgy Votes

## Requesitos
- Docker
- Docker Compose
- Java 8
- Maven 3.X

### Docker
Pretendo disponibilizar a aplicação em containers

Dependência
```sudo docker run --name mongo -d -p 27017:27017 mongo```

API
Antes de construir a imagem devemos fazer um build do maven  
```mvn clean install```

```sudo docker build -t entelgy-votes-api . ```

```sudo docker run --name=entelgy-votes-api -d -p 8888:8080 --net=host -e "PROFILE=dev" -e "SERVER_PORT=8888" entelgy-votes-api```

### Storage
MongoDB pois acredito que atente a necessidade simples de registrar os votos e contá-los.

Poderia seguir uma abordagem de Salvar as capanhas em um banco relacional pois possue maior valor a transação.
Mas, optei por usar 100% o Mongo e o artifício de aggregation para simplificar o resultado da votação.
Ex.:

```
db.getCollection('vote').aggregate([{$match:{"campaignId":"57f1a613df680e2e831b03c7"}},{$group:{_id:"$candidate", total_de_votos: {$sum:1}}}])
```

### Back End
- Spring Boot
  - Spring Starter Data Mongo
  - Spring Starter Web (Rest API)
- Swagger : API Live Documentation
  - [http://localhost:8080/swagger-ui.html](http://localhost:8080/swagger-ui.html)

## Para setar a massa de dados inicial

### Acesso a API Swagger
```
http://localhost:8888/swagger-ui.html
```

Em http://localhost:8888/swagger-ui.html#!/campaign/createUsingPOST e utilize o payload de Campaign:

```
{
    "name" : "Melhor foto",
    "active" : true,
    "candidates" : [ 
        {
            "name" : "Sophia 1",
            "url" : "https://scontent.fplu1-1.fna.fbcdn.net/v/t1.0-0/p206x206/14440945_1038014509601511_8066276063511950224_n.jpg?oh=b273285178a9f7345fe6d1b78e8abadb&oe=58656301"
        }, 
        {
            "name" : "Sophia 2",
            "url" : "https://scontent.fplu1-1.fna.fbcdn.net/v/t1.0-9/13921181_1003625076373788_8227879464644731029_n.jpg?oh=630b797c1d9ee2b943e0d7edd167685e&oe=58ABE2E7"
        }
    ]
}
```

Ou 

## CURL

Parac criar a campanha inicial do sistema execute o curl ou utilize

```
curl -X POST --header "Content-Type: application/json" --header "Accept: */*" -d "{
   \"name\" : \"Melhor foto\",
    \"active\" : true,
    \"candidates\" : [ 
        {
            \"name\" : \"Sophia 1\",
            \"url\" : \"https://scontent.fplu1-1.fna.fbcdn.net/v/t1.0-0/p206x206/14440945_1038014509601511_8066276063511950224_n.jpg?oh=b273285178a9f7345fe6d1b78e8abadb&oe=58656301\"
        }, 
        {
            \"name\" : \"Sophia 2\",
            \"url\" : \"https://scontent.fplu1-1.fna.fbcdn.net/v/t1.0-9/13921181_1003625076373788_8227879464644731029_n.jpg?oh=630b797c1d9ee2b943e0d7edd167685e&oe=58ABE2E7\"
        }
    ]
}" "http://localhost:8888/campaign"
```

## Coisas que abri mão devido ao tempo

- Melhor estratégia de testes de integração automatizados. Fiz apenas 1 de integração e 1 unitário.
- Logs 
- Correto tratamento de provavel exceptions
- De/Para de Doman para Entity para evitar Mass Assigment [https://www.owasp.org/index.php/Mass_Assignment_Cheat_Sheet](https://www.owasp.org/index.php/Mass_Assignment_Cheat_Sheet)
- Tratamento de CORS eu deixei livre todos os domínios, mas o correto é filtrar de forma específica em cenários de produção.

