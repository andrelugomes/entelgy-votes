package br.com.andrelugomes.api.business;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.BDDMockito.given;

import java.util.Arrays;

import br.com.andrelugomes.api.domain.CampaignAggregation;
import br.com.andrelugomes.api.domain.CampaignSumarry;
import br.com.andrelugomes.api.repository.CampaignRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment= SpringBootTest.WebEnvironment.RANDOM_PORT)
public class CampaignBusinessTest {

	@Autowired
	private CampaignBusiness business;

	@MockBean
	private CampaignRepository campaignRepository;

	private String id;

	@Before
	public void setUp() {
		id = "98754saa657wwr";
		given(campaignRepository.aggregation(id)).willReturn(Arrays.asList(new CampaignAggregation("Teste 1", 2), new CampaignAggregation("Teste 2", 4)));
	}

	@Test
	public void shouldGetAllVotesAggregationSumary() throws Exception {
		final CampaignSumarry campaignSumarry = business.votesAggregationByCampaign(id);

		assertThat(campaignSumarry.getSum(), is(Long.valueOf(6)));
		assertThat(campaignSumarry.getResult().size(), is(2));
	}
}
