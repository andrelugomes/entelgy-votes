package br.com.andrelugomes.api.controller;

import br.com.andrelugomes.api.domain.Vote;
import br.com.andrelugomes.api.repository.VoteRepository;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.embedded.EmbeddedWebApplicationContext;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.client.RestTemplate;

import java.time.LocalDateTime;
import java.time.Month;

import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment= SpringBootTest.WebEnvironment.RANDOM_PORT)
public class VoteControllerTest {

	private Vote vote;
	private MockMvc mockMvc;

	@Autowired
	private EmbeddedWebApplicationContext webApplicationContext;

	@Autowired
	private ObjectMapper mapper;

	@MockBean
	private VoteRepository repository;

	@Before
	public void setUp() {
		mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
		vote = new Vote();
		vote.setCampaignId("57f2e878a878301812170f0a");
		vote.setCandidate("Andre");
		vote.setDate(LocalDateTime.of(2016, Month.OCTOBER, 3, 12, 00));

		given(repository.save(vote)).willReturn(vote);
	}


	@Test
	public void shouldRegisterANewVote() throws Exception {
		String data = mapper.writeValueAsString(vote);
		mockMvc.perform(post("/votes").contentType(MediaType.APPLICATION_JSON).content(data))
				.andExpect(status().isCreated());
	}
}
