package br.com.andrelugomes.api.domain;

import java.time.LocalDateTime;

import org.springframework.data.mongodb.core.mapping.Document;

/**
 * Created by andre on 01/10/16.
 */
@Document
public class Vote {

    private String candidate;
    private LocalDateTime date;
    private String campaignId;

    public void setCandidate(String candidate) {
        this.candidate = candidate;
    }

    public void setDate(LocalDateTime date) {
        this.date = date;
    }

    public String getCandidate() {
        return candidate;
    }

    public LocalDateTime getDate() {
        return date;
    }

    public void setCampaignId(String campaignId) {
        this.campaignId = campaignId;
    }

    public String getCampaignId() {
        return campaignId;
    }
}
