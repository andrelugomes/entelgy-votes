package br.com.andrelugomes.api.repository;

import java.util.List;

import br.com.andrelugomes.api.domain.CampaignAggregation;

/**
 * Created by andre on 02/10/16.
 */
public interface CampaignVotesAggregationRepository {

    List<CampaignAggregation> aggregation(String campaignId);
}
