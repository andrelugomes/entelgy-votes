package br.com.andrelugomes.api.domain;

import java.io.Serializable;

/**
 * Created by andre on 03/10/16.
 */
public class Candidates implements Serializable {

    private String name;
    private String url;

    public String getName() {
        return name;
    }

    public String getUrl() {
        return url;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
