package br.com.andrelugomes.api.domain;

import java.io.Serializable;

/**
 * Created by andre on 02/10/16.
 */
public class CampaignAggregation implements Serializable{

    private String candidate;
    private long total;

    public CampaignAggregation (String candidate, long total) {
        this.candidate = candidate;
        this.total = total;
    }

    public String getCandidate() {
        return candidate;
    }

    public long getTotal() {
        return total;
    }

    public void setCandidate(String candidate) {
        this.candidate = candidate;
    }

    public void setTotal(long total) {
        this.total = total;
    }
}
