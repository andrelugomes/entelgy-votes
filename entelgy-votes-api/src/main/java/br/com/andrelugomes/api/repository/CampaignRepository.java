package br.com.andrelugomes.api.repository;

import br.com.andrelugomes.api.domain.Campaign;
import br.com.andrelugomes.api.domain.Vote;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Optional;

/**
 * Created by andre on 01/10/16.
 */
@Repository
public interface CampaignRepository extends MongoRepository<Campaign, String>, CampaignVotesAggregationRepository {

    @Query("{'active' : true}")
    public Optional<Campaign> findActiveCampaign();
}
