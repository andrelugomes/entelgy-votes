package br.com.andrelugomes.api.business;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import br.com.andrelugomes.api.domain.Campaign;
import br.com.andrelugomes.api.domain.CampaignAggregation;
import br.com.andrelugomes.api.domain.CampaignSumarry;
import br.com.andrelugomes.api.exception.CampignNotFoundException;
import br.com.andrelugomes.api.repository.CampaignRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

/**
 * Created by andre on 02/10/16.
 */
@Service
public class CampaignBusiness {

    @Autowired
    private CampaignRepository campaignRepository;

    public Campaign save(Campaign campaign) {
        return campaignRepository.save(campaign);
    }

    public Campaign findByActive() {
        return campaignRepository.findActiveCampaign().orElseThrow(
                () -> new CampignNotFoundException("Campanha não encontrada")
        );
    }

    public CampaignSumarry votesAggregationByCampaign(String id) {
        List<CampaignAggregation> result = campaignRepository.aggregation(id).stream().collect(Collectors.toList());
        //result.sort((v1,v2) -> (int) (v1.getTotal() - v2.getTotal()));
        result.sort(Comparator.comparing(CampaignAggregation::getTotal));
        final long sum = result.stream().mapToLong(c -> c.getTotal()).sum();

        return new CampaignSumarry(result,sum);
    }

    public Page<Campaign> findAll(Pageable pageable) {
        return campaignRepository.findAll(pageable);
    }
}
