package br.com.andrelugomes.api.controller;

import br.com.andrelugomes.api.business.CampaignBusiness;
import br.com.andrelugomes.api.domain.Campaign;
import br.com.andrelugomes.api.domain.CampaignSumarry;
import br.com.andrelugomes.api.domain.Vote;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by andre on 01/10/16.
 */
@Api(value = "/campaign", description = "Serviço REST de Campanhas")
@RestController
@RequestMapping(value = "/campaign")
public class CampaignController {

    @Autowired
    private CampaignBusiness business;

    @ApiOperation(value = "Serviço para criar Campanha", httpMethod = "POST", notes ="Serviço para criar Campanha<br/><pre>"
            + //
            "{\n" + //
            " \"name\":\"NOME_DA_CAMPANHA\",\n" +
            " \"active\":\"true/false\", \n" +
            " \"candidates\":[{\"name\":\"André\",\"url\":\"http://teste\"},{ \"name\":\"Marcos\", \"url\":\"http://teste\"}] \n" +
            "}</pre>" )
    @ApiImplicitParams({
            @ApiImplicitParam(name = "campaign", value = "Campanha", required = true)
    })
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Created", response = Vote.class),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 500, message = "Failure")})
    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<Campaign> create(@RequestBody final Campaign campaign){
        Campaign created = business.save(campaign);
        return new ResponseEntity<Campaign>(created, HttpStatus.CREATED);
    }

    @ApiOperation(value = "Serviço para recuperar a Campanha Ativa", httpMethod = "GET",
            notes ="Serviço para recuperar a Campanha Ativa<br/><pre>" + //
            "{\n" + //
            " \"page\":\"0\",\n" +
            " \"size\":\"10\", \n" +
            " \"sort\":\"candidate,desc\" \n" +
            "}</pre>" )
    @ApiImplicitParams({
            @ApiImplicitParam(name = "pageable", value = "Pageable's model", required = true)
    })
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Success", response = Page.class),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 500, message = "Failure")})
    @RequestMapping(value = "/active",method = RequestMethod.GET)
    public ResponseEntity<Campaign> activeCampaign(){
        return new ResponseEntity<Campaign>(business.findByActive(), HttpStatus.OK);
    }

    @ApiOperation(value = "Serviço para recuperar Votos agregados por Campanha", httpMethod = "GET",
            notes ="Serviço para recuperar Votos agregados por Campanha<br/><pre>"+ //
                    "{\n" + //
                    " \"page\":\"0\",\n" +
                    " \"size\":\"10\", \n" +
                    " \"sort\":\"candidate,desc\" \n" +
                    "}</pre>" )
    @ApiImplicitParams({
            @ApiImplicitParam(name = "pageable", value = "Pageable's model", required = true)
    })
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Success", response = Page.class),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 500, message = "Failure")})
    @RequestMapping(value = "/{id}/aggregate" ,method = RequestMethod.GET)
    public ResponseEntity<CampaignSumarry> aggregateVotesByCampaign(@ApiParam(name = "id") @PathVariable String id){
        return new ResponseEntity<CampaignSumarry>(business.votesAggregationByCampaign(id), HttpStatus.OK);
    }

    @ApiOperation(value = "Serviço para recuperar Campanhas", httpMethod = "GET",
            notes ="Serviço para recuperar Campanhas<br/><pre>" + //
                    "{\n" + //
                    " \"page\":\"0\",\n" +
                    " \"size\":\"10\", \n" +
                    " \"sort\":\"candidate,desc\" \n" +
                    "}</pre>" )
    @ApiImplicitParams({
            @ApiImplicitParam(name = "pageable", value = "Pageable's model", required = true)
    })
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Success", response = Page.class),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 500, message = "Failure")})
    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity<Page<Campaign>> campaigns(Pageable pageable){
        return new ResponseEntity<Page<Campaign>>(business.findAll(pageable), HttpStatus.OK);
    }
}
