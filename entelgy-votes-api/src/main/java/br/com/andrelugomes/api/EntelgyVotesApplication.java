package br.com.andrelugomes.api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EntelgyVotesApplication {

	public static void main(String[] args) {
		SpringApplication.run(EntelgyVotesApplication.class, args);
	}

}
