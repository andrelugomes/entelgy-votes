package br.com.andrelugomes.api.domain;

import java.io.Serializable;
import java.util.List;

/**
 * @author s2it_agomes
 * @version $Revision: $<br/>
 *          $Id: $
 * @since 10/6/16 12:59 PM
 */
public class CampaignSumarry implements Serializable{

    private long sum;

    private List<CampaignAggregation> result;

    public CampaignSumarry (final List<CampaignAggregation> result, final long sum) {
        this.result = result;
        this.sum = sum;
    }

    public void setResult (final List<CampaignAggregation> result) {
        this.result = result;
    }

    public void setSum (final long sum) {
        this.sum = sum;
    }

    public long getSum () {
        return sum;
    }

    public List<CampaignAggregation> getResult () {
        return result;
    }
}
