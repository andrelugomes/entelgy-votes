package br.com.andrelugomes.api.domain;

import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * Created by andre on 02/10/16.
 */
@Document
public class Campaign {

    @Id
    private String id;
    private String name;
    private Boolean active;
    private List<Candidates> candidates;

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public Boolean getActive() {
        return active;
    }

    public List<Candidates> getCandidates() {
        return candidates;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public void setCandidates(List<Candidates> candidates) {
        this.candidates = candidates;
    }
}
