package br.com.andrelugomes.api.repository;

import br.com.andrelugomes.api.domain.Vote;
import org.springframework.data.mongodb.repository.MongoRepository;

/**
 * Created by andre on 01/10/16.
 */
public interface VoteRepository extends MongoRepository<Vote, String> {
}
