package br.com.andrelugomes.api.repository;

import static org.springframework.data.mongodb.core.aggregation.Aggregation.group;
import static org.springframework.data.mongodb.core.aggregation.Aggregation.match;
import static org.springframework.data.mongodb.core.aggregation.Aggregation.project;
import static org.springframework.data.mongodb.core.query.Criteria.where;

import java.util.List;

import br.com.andrelugomes.api.domain.Vote;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import org.springframework.data.mongodb.core.aggregation.GroupOperation;
import org.springframework.data.mongodb.core.aggregation.MatchOperation;
import org.springframework.data.mongodb.core.aggregation.ProjectionOperation;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.stereotype.Repository;

import br.com.andrelugomes.api.domain.CampaignAggregation;

/**
 * Created by andre on 02/10/16.
 */
@Repository
public class CampaignRepositoryImpl implements CampaignVotesAggregationRepository {

    @Autowired
    private MongoTemplate mongoTemplate;

    /**
     *
     * db.getCollection('vote').aggregate([{$match:{"campaignId":"57f1a613df680e2e831b03c7"}},{$group:{_id:"$candidate", total_de_votos: {$sum:1}}}])
     *
     */

    @Override
    public List<CampaignAggregation> aggregation(String campaignId) {
        MatchOperation matchOperation = getMatchOperation(campaignId);
        GroupOperation groupOperation = getGroupOperation();
        ProjectionOperation projectionOperation = getProjectOperation();
        return mongoTemplate.aggregate(
                Aggregation.newAggregation(matchOperation,groupOperation,projectionOperation), Vote.class, CampaignAggregation.class).getMappedResults();
    }

    private MatchOperation getMatchOperation(String campaignId) {
        Criteria priceCriteria = where("campaignId").is(campaignId);
        return match(priceCriteria);
    }

    private GroupOperation getGroupOperation() {
        return group("candidate").last("candidate").as("candidate").count().as("total");
    }

    private ProjectionOperation getProjectOperation() {
        return project("candidate", "total").and("campaignId").previousOperation();
    }
}
