package br.com.andrelugomes.api.controller;

import br.com.andrelugomes.api.domain.Vote;
import br.com.andrelugomes.api.repository.VoteRepository;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by andre on 01/10/16.
 */
@Api(value = "/votes", description = "Serviço REST de Votos")
@RestController
@RequestMapping(value = "/votes")
public class VoteController {

    @Autowired
    private VoteRepository repository;

    @ApiOperation(value = "Serviço para enviar Votos", httpMethod = "POST", notes ="Serviço para enviar Votos<br/><pre>"
            + //
            "{\n" + //
            "  \"candidate\":\"ANDRE\",\n" +
            "  \"date\":\"2016-01-23T00:00:00\", \n" +
            "  \"campaignId\":\"57f1a613df680e2e831b03c7\" \n" +
            "}</pre>" )
    @ApiImplicitParams({
            @ApiImplicitParam(name = "vote", value = "Voto", required = true)
    })
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Created", response = Vote.class),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 500, message = "Failure")})
    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<Vote> vote(@RequestBody final Vote vote){
        Vote voted = repository.save(vote);
        return new ResponseEntity<Vote>(voted, HttpStatus.CREATED);
    }

    @ApiOperation(value = "Serviço para recuperar Votos", httpMethod = "GET",
            notes ="Serviço para recuperar de forma paginada Votos<br/><pre>" + //
            "{\n" + //
            " \"page\":\"0\",\n" +
            " \"size\":\"10\", \n" +
            " \"sort\":\"candidate,desc\" \n" +
            "}</pre>" )
    @ApiImplicitParams({
            @ApiImplicitParam(name = "pageable", value = "Pageable's model", required = true)
    })
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Success", response = Page.class),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 500, message = "Failure")})
    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity<Page<Vote>> votes(Pageable pageable){
        return new ResponseEntity<Page<Vote>>(repository.findAll(pageable), HttpStatus.OK);
    }
}
