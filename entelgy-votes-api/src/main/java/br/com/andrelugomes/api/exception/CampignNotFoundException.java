package br.com.andrelugomes.api.exception;

/**
 * Created by andre on 03/10/16.
 */
public class CampignNotFoundException extends RuntimeException {
    public CampignNotFoundException(String message) {
        super(message);
    }
}
