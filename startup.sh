#!/bin/sh

set -e

echo "Criando campanha inicial"
curl -X POST --header "Content-Type: application/json" --header "Accept: */*" -d "{
   \"name\" : \"Melhor foto\",
    \"active\" : true,
    \"candidates\" : [ 
        {
            \"name\" : \"Sophia 1\",
            \"url\" : \"https://scontent.fplu1-1.fna.fbcdn.net/v/t1.0-0/p206x206/14440945_1038014509601511_8066276063511950224_n.jpg?oh=b273285178a9f7345fe6d1b78e8abadb&oe=58656301\"
        }, 
        {
            \"name\" : \"Sophia 2\",
            \"url\" : \"https://scontent.fplu1-1.fna.fbcdn.net/v/t1.0-9/13921181_1003625076373788_8227879464644731029_n.jpg?oh=630b797c1d9ee2b943e0d7edd167685e&oe=58ABE2E7\"
        }
    ]
}" "http://localhost:8888/campaign"

