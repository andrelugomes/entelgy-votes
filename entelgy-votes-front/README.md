# Entelgy Votes WebApp

## Requesitos
- NodeJS
- npm

## install http-server
```
sudo npm install http-server -g
```


## Running
```
http-server -p 8081


http://localhost:8081/
```

# A aplicação
Para o front-end, decidi usar AngularJS na versão 1.4 como framework e fazer chamadas REST para a API.

Como segurança de votação, coloquei o ReCaptcha do Google para prevenir votações automáticas por robô 
[https://www.google.com/recaptcha](https://www.google.com/recaptcha).

Esta fixo no codigo o host da API *http://localhost:8888*, em um cenário de projeto real deveria pensar em como deixar o host da API em um único lugar.


## Com Docker

```
sudo docker build -t entelgy-votes-front .
```

```
sudo docker run --name=entelgy-votes-front -d -p 8081:8081 --net=host entelgy-votes-front
```




