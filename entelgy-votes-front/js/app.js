angular.module('votesApp', ['noCAPTCHA'])
.config(['noCAPTCHAProvider',
        function (noCaptchaProvider){
          noCaptchaProvider.setSiteKey('6LcwawgUAAAAABxKtcmLOmOwS-mAZ9pO4SbPeKJx');
          noCaptchaProvider.setTheme('dark');
        }
      ])
.controller('CampaignController',function($scope, $http, $filter,$window) {
    $http.get('http://localhost:8888/campaign/active')
        .then(function(response) {
            $scope.campaign = response.data;
            $scope.candidates = $scope.campaign.candidates;

            $scope.gRecaptchaResponse = '';

            $scope.$watch('gRecaptchaResponse', function (){
                $scope.expired = false;
            });
            $scope.expiredCallback = function expiredCallback(){
                $scope.expired = true;
            };

            $scope.vote = function(candidate, campaignId){

              if($scope.gRecaptchaResponse==''){
                console.log($scope.gRecaptchaResponse)
                alert("Confirmação de humano necessária! Mr. Robot!")
                return;
              }
              //"2016-01-23T00:00:00"
              var vote = { 
                'candidate' : candidate,
                'date':$filter('date')(new Date(), "yyyy-MM-ddTHH:mm:ss"),
                'campaignId':campaignId
              }
              console.log(vote);
              $http.post('http://localhost:8888/votes',vote)
                .then(function(response) {
                    if(response){
                      $window.location.href = '/results.html';
                    }
                });
              }
            
        });
})
.controller('ResultController',function($scope, $http) {
  $http.get('http://localhost:8888/campaign/active')
        .then(function(response) {
            var campaignId = response.data.id;  
        $http.get('http://localhost:8888/campaign/'+campaignId+'/aggregate')
            .then(function(response) {
                $scope.sumary = response.data;
            });
      });
});