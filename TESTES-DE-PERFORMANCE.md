# Testes de Performance

Para realizar testes de performance na aplicação, usei uma ferramente chamada [https://github.com/wg/wrk](wrk). Esta ferramente permite de forma simples por linha de comando executar testes de carga na aplicação.

## Instalando o wrk

```
sudo apt install wrk

wrk -v
```

## Usando 

```
$ wrk -t12 -c400 -d30s http://localhost:8081/
```

### Resultado

```
Running 30s test @ http://localhost:8081/
  12 threads and 400 connections
  Thread Stats   Avg      Stdev     Max   +/- Stdev
    Latency   486.70ms   49.53ms 851.50ms   71.23%
    Req/Sec    93.16     84.74   323.00     75.65%
  23840 requests in 30.09s, 69.53MB read
Requests/sec:    792.26
Transfer/sec:      2.31MB
```

- -t12 : Indica 12 threads rodando.
- -c400 : Indica 400 conexões abertas.
- -d5s : Indica que o teste ficou em execução por 30 segundos.

Como resultado podemos ver a quantidade de requisições realizadas no período, a média e desvio padrão da latência, dentre outros resultados.

