# Entelgy votes

Estas instruções são para rodar todo a infra-estrutura do projeto. Individualmente cada projeto possui seu README.md com detalhes.


## Requisitos
- Docker : https://docs.docker.com/engine/installation/linux/ubuntulinux/
- Docker Compose : https://docs.docker.com/compose/install/

## Preparação do Ambiente


#### Clone
```
git clone https://andrelugomes@bitbucket.org/andrelugomes/entelgy-votes.git
```

#### Start da Infra-estrutura
``` 
sudo docker-compose up -d 
```

#### Criação da massa de dados inicial
```
./startup.sh
```


# Aplicação Web

## Front End
```
http://localhost:8081/
```

## API Rest com Swagger
``` 
http://localhost:8888/swagger-ui.html
```